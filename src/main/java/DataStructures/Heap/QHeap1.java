package DataStructures.Heap;

public class QHeap1 {

    public static void main(String[] args) {
        testBinaryHeap();

    }

    public static void testBinaryHeap() {
        BinaryHeap binaryHeap = new BinaryHeap();
        binaryHeap.insert(1);
        binaryHeap.printHeap();
        binaryHeap.insert(3);
        binaryHeap.printHeap();
        binaryHeap.insert(2);
        binaryHeap.printHeap();
        binaryHeap.insert(100);
        binaryHeap.printHeap();
        System.out.printf("extracted %d\n", binaryHeap.extract());
        binaryHeap.printHeap();
        System.out.printf("extracted %d\n", binaryHeap.extract());
        binaryHeap.printHeap();
        System.out.printf("extracted %d\n", binaryHeap.extract());
        binaryHeap.printHeap();
        System.out.printf("extracted %d\n", binaryHeap.extract());
        binaryHeap.printHeap();
    }
}
