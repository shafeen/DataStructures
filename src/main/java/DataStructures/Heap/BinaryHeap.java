package DataStructures.Heap;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

// TODO: add this class to GitLab and create some tests for it to run in GitLab CI

// NOTE: defaults to a max heap and only works for
// TODO: need to add a comparator
// TODO: need to make this class more generic
public class BinaryHeap {

    private List<Integer> heapArray;
    private Integer root;

    public BinaryHeap() {
        this.heapArray = new ArrayList<>(100);
    }

    public int size() {
        return this.heapArray.size();
    }

    public BinaryHeap insert(int num) {
        /* TODO: complete this -> put it in the bottom and shift up */
        this.heapArray.add(num);

        int endIdx = this.heapArray.size() - 1;
        this.shiftUp(endIdx);
        return this;
    }

    public int extract() {
        /* TODO: complete this -> put the bottom on the top and shift down */
        if (this.heapArray.size() == 0) {
            throw new RuntimeException("Cannot extract element from an empty Heap!");
        }

        if (this.heapArray.size() > 1) {
            swapPositions(0, this.heapArray.size()-1);
        }
        Integer root = this.heapArray.remove(this.heapArray.size() - 1);
        shiftDown(0);
        return root;
    }

    private void shiftUp(int index) {
        /* TODO: complete this */
        int parentIdx = this.getParentIdx(index);
        int parentVal = this.heapArray.get(parentIdx);
        if (this.heapArray.get(index) > parentVal) {
            swapPositions(index, parentIdx);
            shiftUp(parentIdx);
        }
    }

    private void shiftDown(int index) {
        /* TODO: test this -> watch out for out of bounds exceptions */
        Integer leftIdx = 2 * index + 1, rightIdx = 2 * index + 2;

        Integer left = (leftIdx < this.heapArray.size()) ? this.heapArray.get(leftIdx) : null;
        Integer right = (rightIdx < this.heapArray.size())? this.heapArray.get(rightIdx) : null;
        Integer maxChild = getMax(left, right);

        if (maxChild != null && this.heapArray.get(index) < maxChild) {
            Integer maxChildIdx = maxChild.equals(left)? leftIdx : rightIdx;
            swapPositions(maxChildIdx, index);
            shiftDown(maxChildIdx);
        }
    }

    private void swapPositions(int idx1, int idx2) {
        int temp1 = this.heapArray.get(idx1);
        this.heapArray.set(idx1, this.heapArray.get(idx2));
        this.heapArray.set(idx2, temp1);
    }

    private int getParentIdx(int idx) {
        return (idx - 1) / 2;
    }

    public void printHeap() {
        System.out.println(this.heapArray);
    }

    private Integer getMax(Integer i1, Integer i2) {
        if (i1 != null && i2 != null) {
            return Integer.max(i1, i2);
        } else if (i1 == null && i2 != null) {
            return i2;
        } else if (i2 == null && i1 != null) {
            return i1;
        } else {
            return null;
        }
    }
}
