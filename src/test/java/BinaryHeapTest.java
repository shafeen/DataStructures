import DataStructures.Heap.BinaryHeap;
import junit.framework.TestCase;

public class BinaryHeapTest extends TestCase {

    private BinaryHeap heap;

    @Override
    public void setUp() throws Exception {
        this.heap = new BinaryHeap();
    }

    public void testBasicInsert() throws Exception {
        assertEquals(0, this.heap.size());

        this.heap.insert(1);
        assertEquals(1, this.heap.size());

        this.heap.insert(2).insert(3);
        assertEquals(3, this.heap.size());
    }

    public void testBasicExtract() throws Exception {
        int numToInsert1 = 100;
        this.heap.insert(numToInsert1);
        assertEquals(numToInsert1, this.heap.extract());

        int numToInsert2 = 101;
        this.heap.insert(numToInsert2);
        assertEquals(numToInsert2, this.heap.extract());
    }

    public void testExtract() throws Exception {
        this.heap.insert(1).insert(3).insert(2).insert(0);
        assertEquals(3, this.heap.extract());
        assertEquals(2, this.heap.extract());
        assertEquals(1, this.heap.extract());
        assertEquals(0, this.heap.extract());
    }

}
